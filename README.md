# Archetype POC (SEEF-305)

POC de um archetype do Maven para projetos spring-boot, utilizando bibliotecas homologadas do experian-base-pom.

### Features

* Suporte a *spring-boot 1.5.10*
* Geração de código via *swagger codegen*
* Autorização via *experian-spring-security-configuration*
* ESAPI (Veracode CWE 117)
* Dockerfile (docker-registry-default.apps.appcanvas.net)

### Modules

* experian-archetype-rest: Archetype spring-boot + docker
* experian-spring-boot-base-pom: Base pom com versões gerenciadas pelo **spring-boot**

### Build

1. parent pom.xml:

```sh
cd experian-spring-boot-base-pom
mvn clean install
```

2. archetype local:

```sh
cd experian-archetype-rest
mvn clean install
```

### Usage

`mvn archetype:generate -DarchetypeCatalog=local -DartifactId=experian-test-service -DgroupId=br.com.experian -Dversion=1.0.0-SNAPSHOT`

### Run

`mvn -f rest/pom.xml spring-boot:run -DJWT_SIGNING_KEY=jwt_key`

`docker run --rm -it -e "AWS_DEFAULT_REGION=sa-east-1" <image_name>`

View [actuator](http://localhost:8080/health) report.

### Links

##### Dependencies:
* [Spring-boot dependency management](https://github.com/spring-projects/spring-boot/blob/1.5.x/spring-boot-dependencies/pom.xml)

##### Archetype:
* [Maven Archetype Plugin](http://maven.apache.org/archetype/maven-archetype-plugin/advanced-usage.html)

##### Dockerfile:
* [Spring-boot Docker](https://spring.io/guides/gs/spring-boot-docker/)
* [JDK opts](https://blogs.oracle.com/java-platform-group/java-se-support-for-docker-cpu-and-memory-limits)

##### Code style:
* [GPD/DA GSG Coding Standards](http://confluenceglobal.experian.local/confluence/display/GPD/DA+GSG+Coding+Standards+and+Code+Quality)
* [RB/CrossCore Development Practices](http://confluenceglobal.experian.local/confluence/display/RB/CrossCore+Core+Team+Development+Practices)