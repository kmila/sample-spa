#!/bin/sh

if [ ${AWS_CONTAINER_CREDENTIALS_RELATIVE_URI} ]
then
	aws ssm get-parameters --names ${PARAMETER_ECS} --with-decryption | jq --raw-output .Parameters[0].Value > /.env
	. ./.env

	rm -rf .env
fi

java $JAVA_OPTS -Dspring.profiles.active=${SPRING_PROFILE} -Dlogging.level.=${SPRING_LOGGING_LEVEL} --server.port=${PORT} -jar /app/app.jar