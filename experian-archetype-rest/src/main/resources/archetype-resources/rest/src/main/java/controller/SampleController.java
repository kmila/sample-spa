#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.controller;

import ${package}.rest.api.TestAccountsApi;
import ${package}.rest.model.UserAccount;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/internal/sample/v1")
public class SampleController implements TestAccountsApi {

  private static final org.owasp.esapi.Logger LOGGER = org.owasp.esapi.ESAPI
      .getLogger(SampleController.class);

  @Override
  @PreAuthorize("hasRole('CLI-AUTH-IDENTIFIED') and hasRole('CLI-1STPARTY') and hasRole('AUTH-BASIC') and hasRole('USER')")
  public ResponseEntity<Void> doUpdate(String authorization, UserAccount userId) {
    return new ResponseEntity<>(HttpStatus.CREATED);
  }

  @Override
  public ResponseEntity<List<UserAccount>> findById(String userId, Integer limit, Integer offset) {
    LOGGER.debug(org.owasp.esapi.Logger.EVENT_UNSPECIFIED, userId);
    UserAccount testAccount = new UserAccount();
    testAccount.setId(userId);
    testAccount.setCreationDate(LocalDateTime.now());
    List<UserAccount> userAccounts = Collections.singletonList(testAccount);
    return new ResponseEntity<>(userAccounts, HttpStatus.CREATED);
  }

}
